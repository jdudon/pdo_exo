# Cours sur PDO en PHP

## Pré requis
### SQL
Vous trouverez ici un fichier db.sql à implémenter afin de créer une Base de donnée sql sur laquelle on pourra travailler.
### PHP
Connaître les bases de la POO PHP.

## Exercice
Le but sera de savoir naviguer à travers notre base de donnée.
- Se connecter à la base de donnée.
1. Naviguer dans la base de donnée et aller chercher toutes les données se trouvant dans la table customers. 
On affichera le tout sous forme de tableau.
2. Afficher les 20 derniers clients de la société (forcément les 20 derniers ajoutés).
On affichera également sous forme de tableau.
3. Afficher uniquement les commandes auxquelles un commentaire a été ajouté et préciser si l'envoi a été effectué ou non.
Sous le format de votre choix
4. Pour chacune de ses commandes, indiquer la date de commande et celle d'envoi.


### Infos

Ce dépot sera mis à jour.

## Méthodes

- PDOStatement::Fetch()
- PDOStatement::FetchAll()
- PDO::Query()
- PDOException::getMessage()


